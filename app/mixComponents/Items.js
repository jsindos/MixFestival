import React, { Component, PropTypes } from 'react'
import { gql, graphql } from 'react-apollo'

class Items extends Component {
  constructDateArray() {
    /**
     * Selector function
     * @param {wp_theater post} e
     * @returns {wp_theater post}
     */
    const getSameListing = (event, e) => e.parent_event.id === event.parent_event.id

    /**
     * Selector function
     * @param {wp_theater post} p
     * @returns {String} event date, of the format 'yyyy-mm-dd hh:mm'
     */
    const getEventDate = p => p.post_meta.find(pp => pp.meta_key === 'event_date').meta_value.split(' ')

    /**
     * Selector function
     * @param {Array} p - Array containing posts of a given date
     * @returns {Boolean} Returns true if the Array contains posts of the same date as `eventDate`
     */
    // const eventsOnToday = p => p.includes(pp => getEventDate(pp)[0] === eventDate)
    const eventsOnToday = (eventDate, p) => p[0].date === eventDate

    return this.props.data.posts && this.props.data.posts.reduce((eventsSortedByDate, event) => {
      let [ eventDate, eventTime ] = getEventDate(event)
      const eventsToday = eventsSortedByDate.find(eventsOnToday.bind(null, eventDate)) || []
      const eventsNotOnToday = eventsSortedByDate.filter(e => !eventsOnToday(eventDate, e))
      const sameListingEvent = eventsToday.find(getSameListing.bind(null, event))
      return eventsNotOnToday.concat([
        sameListingEvent ?
          eventsToday.filter(e => !getSameListing(event, e)).concat(Object.assign({}, sameListingEvent, {
              times: sameListingEvent.times.concat([ eventTime ])
            }
          )) :
          eventsToday.concat([
            Object.assign({}, event, {
              times: [ eventTime ],
              date: eventDate
            })
          ])
      ])
    }, [])
  }

  render() {
    const eventsSortedByDate = this.constructDateArray()
    console.log(eventsSortedByDate)

    return (
      <div>Hello World!</div>
    )
  }
}

Items.propTypes = {
  data: PropTypes.object
}

const ItemsQuery = gql`
  query getItemsQuery{
    posts(post_type: "wp_theatre_event"){
      post_meta {
        meta_id
        post_id
        meta_key
        meta_value
      }
      parent_event {
        id
        post_title
        categories {
          term_id
          name
          slug
        }
      }
    }
  }
`

const ItemsWithData = graphql(ItemsQuery)(Items)

export default ItemsWithData
