const { privateSettings, publicSettings } = require('../settings/settings')
const { WordExpressDefinitions, WordExpressDatabase, WordExpressResolvers } = require('wordexpress-schema')
const { makeExecutableSchema } = require('graphql-tools')

// returns WordExpressDatabase object that has provides connectors to the database
const Database = new WordExpressDatabase({ privateSettings, publicSettings })
const Connectors = Database.connectors

// Reolving functions that use the database connections to resolve GraphQL queries
const Resolvers = WordExpressResolvers(Connectors, publicSettings)

// GraphQL schema definitions
const Definitions = WordExpressDefinitions

const executableSchema = makeExecutableSchema({
  typeDefs: Definitions,
  resolvers: Resolvers
})

module.exports.Connectors = Connectors
module.exports.Resolvers = Resolvers
module.exports.Definitions = Definitions
module.exports.executableSchema = executableSchema
